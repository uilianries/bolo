# Exemplo Sphix

## Bolo de Cenoura

Este é um simples exemplo de como utilizar Sphinx, através de uma receita de bolo.

#### Gerar documentação

    $ make html
    $ firefox build/html/index.html
    
#### Licença
[MIT](LICENSE.md)