.. Bolo documentation master file, created by
   sphinx-quickstart on Mon Mar 19 22:13:16 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Bem-vindo a receita do Bolo de cenoura!
=======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   ingredientes/lista
   preparo/preparo
   resultado

Pesquisa
========

* :ref:`search`
