Preparo
=======

.. _preparo:

Modo de Preparo
---------------

Antes de preparar o bolo, verifique se os  :ref:`ingredientes` estão todos presentes.

Preparo do forno
++++++++++++++++

.. important::

  Para obter uma temperatura adequada, pré-aqueca o forno em **180 ºC**

Preparo da massa
++++++++++++++++

Para produzir a massa do bolo:

1. Adicionar todos os :ref:`ingredientes_massa` no liquidificador, **exceto a farinha**
2. Durante o processo, adicione a farinha aos poucos
3. Aguarde até obter uma massa homogênea

Assar
+++++

Antes de despejar a massa na forma:

1. Unte e farinhe a forma
2. Despeje a massa na forma
3. Coloque a forma no forno já pré-aquecido
4. Deixe assar por **40 minutos**

Preparo da cobertura
++++++++++++++++++++

Enquato a massa assa, prepare a cobertura:

1. Adicionar todos os :ref:`ingredientes_cobertura` em uma panela.
2. Coloque em fogo alto
3. Mexa até ferver
4. Desligue e reserve

Montagem
++++++++

Para finalizar, monte o bolo com a cobertura.
