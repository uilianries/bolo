Ingredientes
============

.. _ingredientes:

Lista de Ingredientes
---------------------

Antes que o :ref:`preparo` seja realizado, alguns importantes ingredientes devem ser separados.


.. _ingredientes_massa:

Ingredientes para massa
+++++++++++++++++++++++

.. code-block:: json
   :caption: **ingredientes.json**

    {"ingrediente": "cenoura", "quantidade": 3}
    {"ingrediente": "ovo", "quantidade": 3}
    {"ingrediente": "óleo de soja", "quantidade": 1, "grandeza": "xícara"}
    {"ingrediente": "açucar", "quantidade": 2, "grandeza": "xícara"}
    {"ingrediente": "farinha de trigo", "quantidade": 2, "grandeza": "xícara"}
    {"ingrediente": "fermento químico", "quantidade": 1, "grandeza": "colher de sopa"}
    {"ingrediente": "sal", "quantidade": 1, "grandeza": "colher de chá"}

.. _ingredientes_cobertura:

Ingredientes para cobertura
+++++++++++++++++++++++++++

A lista de ingredientes é relativa a calda sabor **chocolate**

.. code-block:: json

    {"ingrediente": "açucar", "quantidade": 5, "grandeza": "colher de sopa"}
    {"ingrediente": "chocolate em pó", "quantidade": 3, "grandeza": "colher de sopa"}
    {"ingrediente": "manteiga", "quantidade": 2, "grandeza": "colher de sopa"}
    {"ingrediente": "leite", "quantidade": 2, "grandeza": "colher de sopa"}
